﻿DROP VIEW IF EXISTS INDIRECTUSER;
DROP VIEW IF EXISTS DIRECTUSER;
DROP VIEW AUTHNONOWNER;
/* 
	Find all pairs of the form (user,signer), where user is an authorized user of an organization's
	credit card, signer is a person at that organization with signature authority, and the balance
	on the card is within $1,000 of the credit limit. For each user/signer, show Id and Name (see
	the expected output for an example).
*/

SELECT U.ID AS UserId, U.NAME AS UserName, S.ID AS SignerId, S.NAME AS SignerName 
FROM PERSON U, PERSON S, CREDITCARD C, ORGANIZATION D
WHERE C.CARD_OWNER = D.OID 
      AND (C.CARD_LIMIT - C.BALANCE) <= 1000 
      AND U.ID = ANY(C.AUTHUSER) 
      AND S.ID = ANY(D.SIGNERS);


/*
	Find all users (Id, Name) who own four or more cards and are authorized non-owner users for
	three or more other cards. This query must use aggregates.
*/

CREATE VIEW AUTHNONOWNER (ACCOUNT_NUMBER, NON_OWNER) AS (
SELECT C.ACCOUNT_NUMBER, UNNEST(D.SIGNERS) FROM CREDITCARD C, ORGANIZATION D WHERE (C.card_owner = D.oid)
UNION 
SELECT C.ACCOUNT_NUMBER, UNNEST(C.AUTHUSER) FROM CREDITCARD C, PERSON D WHERE (C.card_owner = D.oid)
UNION
SELECT C.ACCOUNT_NUMBER, UNNEST(C.AUTHUSER) FROM CREDITCARD C, ORGANIZATION D WHERE (C.card_owner = D.oid)
ORDER BY ACCOUNT_NUMBER);


SELECT C.Id, C.Name
FROM CREDITCARD B, PERSON C 
WHERE 
C.OID = B.CARD_OWNER AND C.ID IN (
	SELECT D.NON_OWNER FROM AUTHNONOWNER D 
	GROUP BY D.NON_OWNER 
	HAVING COUNT(D.ACCOUNT_NUMBER) >= 3)
GROUP BY C.Id, C.Name 
HAVING COUNT(B.ACCOUNT_NUMBER) >= 4;


/*
	Find the credit cards (acct. numbers) all of whose signers (i.e., people with signature authority
	of the organizations that own those cards) also own personal credit cards with credit limits at
	least $25,000. This query must use quantifiers.
*/

SELECT CC.ACCOUNT_NUMBER
FROM CREDITCARD CC, ORGANIZATION I
WHERE CC.CARD_OWNER = I.OId
      AND NOT EXISTS (  /*Not exists a signer who does not own a credit card whose limit is >= 25000 */
      SELECT P.Id
      FROM PERSON P
      WHERE P.Id = ANY(I.SIGNERS)  /* For any signer of that organization */ 
            AND NOT EXISTS (       /* Not exists a credit card whose limit is >= 25000 */
	    SELECT C.ACCOUNT_NUMBER
            FROM CREDITCARD C
            WHERE C.CARD_OWNER = P.OID 
                  AND C.CARD_LIMIT >= 25000));

/*	Find all pairs (U,C) where U is an indirect user of the credit card C. (For each user, show Id and
	Name. For credit cards, show the account number.)
	A user U is an indirect user of a credit card C if either
	• U is a direct user, i.e., U is one of the authorized users of C ; or
	• U is a direct user of a credit card C2 and the owner of C2 is an indirect user of C. */

/*Direct users of credit cards */
CREATE VIEW DIRECTUSER(USERID,USERNAME,CARD) AS
SELECT P.Id, P.NAME, N.ACCOUNT_NUMBER
FROM PERSON P, AUTHNONOWNER N
WHERE N.NON_OWNER = P.Id;      

/*Indirect users of credit cards */
CREATE RECURSIVE VIEW INDIRECTUSER(USERID,USERNAME,CARD) AS
SELECT * FROM DIRECTUSER D        
UNION
SELECT D.USERId, D.USERNAME, I.CARD
FROM CREDITCARD C, INDIRECTUSER I, DIRECTUSER D, PERSON P
WHERE D.CARD = C.ACCOUNT_NUMBER
      AND C.CARD_OWNER = P.OID
      AND P.ID = I.USERID; 
      
SELECT * FROM INDIRECTUSER I
ORDER BY I.USERID,I.USERNAME,I.CARD;

/*	Find the total of all balances for the credit cards that have Joe as one of the indirect users. This
	query can (and should) reuse some of the earlier queries. 
*/

SELECT sum(C.balance) AS TOTAL_BALANCE
FROM CREDITCARD C, INDIRECTUSER I
WHERE I.USERNAME = 'Joe'
      AND I.CARD = C.ACCOUNT_NUMBER;

