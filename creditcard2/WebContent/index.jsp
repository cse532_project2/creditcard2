<%@page import="com.creditcard.common.CreditCardDBAccess"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Credit Card Database Project 2</title>
<style>
.navbar {
	display:block;
	width:100%;
	height:30px;
	background-color:purple;
}

.navbar ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
}

.navbar li{
	width:100px;
	float:left;	
}
.navbar li a{
	color: white;
	text-decoration: none;
	text-align: right;
}
table thead th {
	font-weight: bold;
}
</style>
</head>
<body>
<h1>Credit Card Project 2</h1>
<p>
	<h3>Saiyang Qi (110617240)</h3>
	<h3>Jugu Dannie Sundar (110455279)</h3>	
</p>
<div class="navbar">
	<ul>
	  <li><a href="#" id="erdiagram">ER Diagram</a></li>
	  <li><a href="#" id="runqueries">Run Queries</a></li>
	</ul>
</div>
<div id="erdiagramDiv">
	<img src ="images/erdiagram.jpg"/>
</div>
<div id="viewdataDiv" style="display:none">
	
</div>
<div id="runqueriesDiv" style="display:none">
	<div class="querySection" style="float:left;width:40%">
		<br/>
		<div>Query 1
			<div class="disp q1">
				<p>
					Find all pairs of the form (user,signer), where user is an authorized user of an organization's
					credit card, signer is a person at that organization with signature authority, and the balance
					on the card is within $1,000 of the credit limit. For each user/signer, show Id and Name.
				</p>
			</div>
			<span><button id = "desc1">Description</button><button id = "query1">Run Query</button></span>
		</div>
		<hr/>
		<div>Query 2
			<div class="disp q2">
				<p>
				Find all users (Id, Name) who own four or more cards and are authorized non-owner users for
				three or more other cards. This query must use aggregates.
				</p>
			</div>
			<span><button id = "desc2">Description</button><button id = "query2">Run Query</button></span>
		</div>
		<hr/>
		<div>Query 3
			<div style = "display:none" class="disp q3">
				<p>
				Find the credit cards (acct. numbers) all of whose signers (i.e., people with signature authority
				of the organizations that own those cards) also own personal credit cards with credit limits at
				least $25,000. This query must use quantifiers
				</p>
			</div>
			<span><button id = "desc3">Description</button><button id = "query3">Run Query</button></span>
		</div>
		<hr/>
		<div>Query 4
			<div style = "display:none" class="disp q4">
				<p>
				Find all pairs (U,C) where U is an indirect user of the credit card C. (For each user, show Id and
				Name. For credit cards, show the account number.)
				A user U is an indirect user of a credit card C if either
				</p>
				<ul>
					<li>U is a direct user, i.e., U is one of the authorized users of C; or</li>
			 		<li>U is a direct user of a credit card C2 and the owner of C2 is an indirect user of C.</li>
			 	</ul>			
			</div>	
			<span><button id = "desc4">Description</button><button id = "query4">Run Query</button></span>
		</div>
		<hr/>
		<div>Query 5
			<div style = "display:none" class="disp q5">
				<p>
				Find the total of all balances for the credit cards that have Joe as one of the indirect users. This
				query can (and should) reuse some of the earlier queries.
				</p>	
			</div>	
			<span><button id = "desc5">Description</button><button id = "query5">Run Query</button></span>
		</div>
		<hr/>
	</div>
	<div class="resultSection" style="float:left;width:60%">
		<div id="q1div" style = "display:none;" align="center">
			<h4>Query 1 Results</h4>
			<table width="50%" border="1px solid" cellpadding="1" cellspacing="0">
				<thead>
					<tr>
						<th rowspan="2">Record</th>
						<th colspan="2">User</th>
						<th colspan="2">Signer</th>
					</tr>
					<tr>
						<th align="center">Id</th>
						<th align="center">Name</th>
						<th align="center">Id</th>
						<th align="center">Name</th>
					</tr>
				</thead>
				<tbody id="q1body">			
				</tbody>
			</table>
		</div>
		<div id="q2div" style = "display:none" align="center">
			<h4>Query 2 Results</h4>
			<table width="50%" border="1px solid" cellpadding="1" cellspacing="0">
				<thead>
					<tr>
						<th>Record</th>
						<th>User Id</th>
						<th>User Name</th>
					</tr>
				</thead>
				<tbody id="q2body">			
				</tbody>
			</table>
		</div>
		<div id="q3div" style = "display:none" align="center">
			<h4>Query 3 Results</h4>
			<table width="50%" border="1px solid" cellpadding="1" cellspacing="0">
				<thead>
					<tr>
						<th>Record</th>
						<th>Credit Card Number</th>
					</tr>
				</thead>
				<tbody id="q3body">			
				</tbody>
			</table>
		</div>
		<div id="q4div" style = "display:none" align="center">
			<h4>Query 4 Results</h4>
			<table width="50%" border="1px solid" cellpadding="1" cellspacing="0">
				<thead>
					<tr>
						<th>Record</th>
						<th>Indirect User Id</th>
						<th>Indirect User Name</th> 
						<th>Card Number</th>
					</tr>
				</thead>
				<tbody id="q4body">			
				</tbody>
			</table>
		</div>
		<div id="q5div" style = "display:none" align="center">
			<h4>Query 5 Results</h4>
			<table width="50%" border="1px solid" cellpadding="1" cellspacing="0">
				<thead>
					<tr>
						<th>Record</th>
						<th colspan="2">Total Balance</th>
					</tr>
				</thead>
				<tbody id="q5body">			
				</tbody>
			</table>
		</div>
	</div>		
</div>
</body>
<script src="scripts/jquery-1.11.3.min.js"></script>
<script type ="text/javascript">
	/*
		Ajax calls and result handlers
	*/	
	function formatResult(responseData) 
	{		
		var jsObj = JSON.parse(responseData);
		if (jsObj.query == 1)
		{
			$(".resultSection #q1div").show();
			$(".resultSection #q1body").html(formatQ1(jsObj));
		}
		else if (jsObj.query == 2)
		{
			$(".resultSection #q2div").show();
			$(".resultSection #q2body").html(formatQ2(jsObj));
		}
		else if (jsObj.query == 3)
		{
			$(".resultSection #q3div").show();
			$(".resultSection #q3body").html(formatQ3(jsObj));
		}
		else if (jsObj.query == 4)
		{
			$(".resultSection #q4div").show();
			$(".resultSection #q4body").html(formatQ4(jsObj));
		}
		else if (jsObj.query == 5)
		{
			$(".resultSection #q5div").show();
			$(".resultSection #q5body").html(formatQ5(jsObj));
		}
	}
	
	function formatQ1(jsObj) // ui function to format result for query 1
	{
		var html = "";
		var jsdata = jsObj.data;
		for (var j = 0; j < jsdata.length; j++)
		{
			html +=  "<tr>";
			html +=  	"<td align=\"center\">" + (j + 1) + "</td>";
			html +=  	"<td align=\"center\">" + jsdata[j].uid + "</td>";
			html +=  	"<td align=\"center\">" + jsdata[j].uname + "</td>";
			html +=  	"<td align=\"center\">" + jsdata[j].sid + "</td>";
			html +=  	"<td align=\"center\">" + jsdata[j].sname + "</td>";
			html +=  "<\tr>";
		}
		return html;
	}
	
	function formatQ2(jsObj) // ui function to format result for query 2
	{
		var html = "";
		var jsdata = jsObj.data;
		for (var j = 0; j < jsdata.length; j++)
		{
			html +=  "<tr>";
			html +=  	"<td align=\"center\">" + (j + 1) + "</td>";
			html +=  	"<td align=\"center\">" + jsdata[j].personid + "</td>";
			html +=  	"<td align=\"center\">" + jsdata[j].personname + "</td>";
			html +=  "<\tr>";
		}
		return html;
	}
	
	function formatQ3(jsObj) // ui function to format result for query 3
	{
		var html = "";
		var jsdata = jsObj.data;
		for (var j = 0; j < jsdata.length; j++)
		{
			html +=  "<tr>";
			html +=  	"<td align=\"center\">" + (j + 1) + "</td>";
			html +=  	"<td align=\"center\">" + jsdata[j].accountnumber + "</td>";
			html +=  "<\tr>";
		}
		return html;
	}
	
	function formatQ4(jsObj) // ui function to format result for query 4
	{
		var html = "";
		var jsdata = jsObj.data;
		for (var j = 0; j < jsdata.length; j++)
		{
			html +=  "<tr>";
			html +=  	"<td align=\"center\">" + (j + 1) + "</td>";
			html +=  	"<td align=\"center\">" + jsdata[j].uid + "</td>";
			html +=  	"<td align=\"center\">" + jsdata[j].uname + "</td>";
			html +=  	"<td align=\"center\">" + jsdata[j].accountnumber + "</td>";
			html +=  "<\tr>";
		}
		return html;
	}
	
	function formatQ5(jsObj) // ui function to format result for query 5
	{
		var html = "";
		var jsdata = jsObj.data;
		html +=  "<tr>";
		html +=  	"<td align=\"center\">1</td>";
		html +=  	"<td align=\"center\">" + jsdata + "</td>";
		html +=  "<\tr>";
		return html;
	}
		
	function delegatequery(queryNumber) // this method is used to invoke the ajax call to the server 
	{
		hideQDivs();
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (xhttp.readyState == 4 && xhttp.status == 200) {
		    	formatResult(xhttp.responseText);
		    }
		  };
		  xhttp.open("GET", "cc?queryNumber="+queryNumber, true);
		  xhttp.send();
	}
	
	$("#query1").click(function() { // attaching event handler for query 1
		delegatequery(1);
	});
	$("#query2").click(function() { // attaching event handler for query 2
		delegatequery(2);	
		});
	$("#query3").click(function() { // attaching event handler for query 3
		delegatequery(3);
	});
	$("#query4").click(function() { // attaching event handler for query 4
		delegatequery(4);
	});
	$("#query5").click(function() { // attaching event handler for query 5
		delegatequery(5);
	});
	
	
	/*
		UI UX functions
	*/
	function hideQDivs()
	{
		$(".resultSection #q1div").hide();
		$(".resultSection #q2div").hide();
		$(".resultSection #q3div").hide();
		$(".resultSection #q4div").hide();
		$(".resultSection #q5div").hide();
	}
	
	function hideAll(){
		$("#viewdataDiv").hide();
		$("#erdiagramDiv").hide();
		$("#runqueriesDiv").hide();
		$(".disp").hide();
	}
	
	$("#erdiagram").click(
		function() {
			hideAll();
			$("#erdiagramDiv").show();
		}
	);
	$("#viewdata").click(
		function () {
			hideAll();
			$("#viewdataDiv").show();
			delegatequery(0);
		}
	);
	$("#runqueries").click(
		function () {
			hideAll();
			$("#runqueriesDiv").show();
		}
	);
	$("#desc1").click(function () {
		$(".q1").toggle();
	});
	$("#desc2").click(function () {
		$(".q2").toggle();
	});
	$("#desc3").click(function () {
		$(".q3").toggle();
	});
	$("#desc4").click(function () {
		$(".q4").toggle();
	});
	$("#desc5").click(function () {
		$(".q5").toggle();
	});
</script>
</html>