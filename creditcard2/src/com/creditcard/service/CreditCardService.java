/****************************************************************************
CSE532 -- Project 2

File name: 		CreditCardService.java
Author(s): 		Jugu Dannie Sundar (110455279) 
		   		Saiyang Qi (110617240)
Description:  	This class is used to as a service class. This class handles all the
				db calls to and results from CreditCardDBService class.
									
We, pledge our honor that all parts of this project were done by us alone 
and without collaboration with anybody else.
****************************************************************************/

package com.creditcard.service;

import java.sql.Connection;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.creditcard.bo.PersonBO;

public class CreditCardService {
	private CreditCardDBService ccDBService = null; // instance variable to class where all the db methods are defined
	
	/**
	 * The constructor takes the connection object to initialize the member instance object.
	 * @param conn
	 */
	public CreditCardService(Connection conn) {
		this.ccDBService = new CreditCardDBService(conn);
	}
	
	/**
	 * This method would have allowed user to view data of all the tables present in the database.
	 * Not implemented as it is outside the scope of the project
	 * @return
	 */
	private JSONObject getAllData()
	{
		JSONObject jsObject = new JSONObject();
		/*List<OwnerBO> ownerList = ccDBService.getOwnerData();
		List<PersonBO> personList = ccDBService.getPersonData();
		List<OrganizationBO> organizationList = ccDBService.getOrganizationData();
		List<CreditCardBO> creditCardList = ccDBService.getCreditCardList();
		*/
		return jsObject;
	}
	
	/**
	 * This method creates json data format for db query 1 which is sent to the client
	 * @return
	 * @throws Exception
	 */
	private JSONObject getQueryOne() throws Exception
	{
		JSONObject resultObject = new JSONObject();
		JSONArray jsArray = new JSONArray();
		List<PersonBO[]> resultList = ccDBService.query1();
		for (PersonBO[] personArray : resultList)
		{
			PersonBO user = personArray[0];
			PersonBO signer = personArray[1];
			JSONObject userSigner = new JSONObject();
			userSigner.put("uid", user.getId());
			userSigner.put("uname", user.getName());
			userSigner.put("sid", signer.getId());
			userSigner.put("sname", signer.getName());
			jsArray.put(userSigner);
		}
		resultObject.put("data",jsArray);
		resultObject.put("query",1);
		return resultObject;
	}
	
	/**
	 * This method creates json data format for db query 2 which is sent to the client
	 * @return
	 * @throws Exception
	 */
	private JSONObject getQueryTwo() throws Exception
	{
		JSONObject resultObject = new JSONObject();
		JSONArray jsArray = new JSONArray();
		List<PersonBO> resultList = ccDBService.query2();
		for (PersonBO personObject : resultList)
		{
			JSONObject jsObj = new JSONObject();
			jsObj.put("personid",personObject.getId());
			jsObj.put("personname",personObject.getName());
			jsArray.put(jsObj);
		}
		resultObject.put("data",jsArray);
		resultObject.put("query",2);
		return resultObject;
	}
	
	/**
	 * This method creates json data format for db query 3 which is sent to the client
	 * @return
	 * @throws Exception
	 */
	private JSONObject getQueryThree()  throws Exception
	{
		JSONObject resultObject = new JSONObject();
		JSONArray jsArray = new JSONArray();
		List<String> resultList = ccDBService.query3();
		for (String cc : resultList)
		{
			jsArray.put(new JSONObject().put("accountnumber", cc));
		}
		resultObject.put("data",jsArray);
		resultObject.put("query",3);
		return resultObject;
	}
	
	/**
	 * This method creates json data format for db query 4 which is sent to the client
	 * @return
	 * @throws Exception
	 */
	private JSONObject getQueryFour()  throws Exception
	{
		JSONObject resultObject = new JSONObject();
		JSONArray jsArray = new JSONArray();
		List<String[]> resultList = ccDBService.query4();
		for (String data[] : resultList)
		{
			JSONObject jsObj = new  JSONObject();
			jsObj.put("uid", data[0]);
			jsObj.put("uname", data[1]);
			jsObj.put("accountnumber", data[2]);
			jsArray.put(jsObj);
		}
		resultObject.put("data",jsArray);
		resultObject.put("query",4);
		return resultObject;
	}
	
	/**
	 * This method creates json data format for db query 5 which is sent to the client
	 * @return
	 * @throws Exception
	 */
	private JSONObject getQueryFive()  throws Exception
	{
		JSONObject resultObject = new JSONObject();
		double totalBalance = ccDBService.query5();
		resultObject.put("data",totalBalance);
		resultObject.put("query",5);
		return resultObject;
	}
	
	/**
	 * This method is used to invoke the queries for the project from the CreditCardDBService
	 * and transform the db results into jsondata and then return the json string 
	 * @param reqResource
	 * @return
	 */
	public String getQueryResult(RequestedResource reqResource)
	{
		JSONObject resultData = new JSONObject();
		try {
			
			if ( "0".equals(reqResource.getQueryNumber())) {
				resultData = getAllData();
			}
			else if ( "1".equals(reqResource.getQueryNumber())) {
				resultData = getQueryOne();
			}
			else if ( "2".equals(reqResource.getQueryNumber())) {
				resultData = getQueryTwo();
			}
			else if ( "3".equals(reqResource.getQueryNumber())) {
				resultData = getQueryThree();
			}
			else if ( "4".equals(reqResource.getQueryNumber())) {
				resultData = getQueryFour();
			}
			else if ( "5".equals(reqResource.getQueryNumber())) {
				resultData = getQueryFive();
			}
		} catch (Exception e){
			try {
				resultData.put("error",e.getMessage());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return resultData.toString();
	}

}
