/****************************************************************************
CSE532 -- Project 2

File name: 		RequestedResource.java
Author(s): 		Jugu Dannie Sundar (110455279) 
		   		Saiyang Qi (110617240)
Description:  	This class is used to capture request parameters from client 
				and convert them into java variables. This is provided for extensibility.
					
					
We, pledge our honor that all parts of this project were done by us alone 
and without collaboration with anybody else.
****************************************************************************/

package com.creditcard.service;

import javax.servlet.http.HttpServletRequest;


public class RequestedResource {

	private String queryNumber;

	public String getQueryNumber() {
		return queryNumber;
	}


	public void setQueryNumber(String queryNumber) {
		this.queryNumber = queryNumber;
	}


	public RequestedResource(HttpServletRequest request) {
		String queryNumber = request.getParameter("queryNumber");
		if (queryNumber == null) queryNumber = "1";
		this.setQueryNumber(queryNumber);
	}
}
