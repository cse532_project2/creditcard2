/****************************************************************************
CSE532 -- Project 2

File name: 		CreditCardDBAccess.java
Author(s): 		Jugu Dannie Sundar (110455279) 
		   		Saiyang Qi (110617240)
Description:  	This is main class which will 
			  	execute all the queries required for the project
					
					
We, pledge our honor that all parts of this project were done by us alone 
and without collaboration with anybody else.
****************************************************************************/

package com.creditcard.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.creditcard.bo.CreditCardBO;
import com.creditcard.bo.OrganizationBO;
import com.creditcard.bo.OwnerBO;
import com.creditcard.bo.PersonBO;

public class CreditCardDBService {

	private Connection connection = null;
	private PreparedStatement pstmt = null;
	private ResultSet rS = null;
	
	public CreditCardDBService(Connection con) {
		this.connection = con;
	}
	
	/* 
	Find all pairs of the form (user,signer), where user is an authorized user of an organization's
	credit card, signer is a person at that organization with signature authority, and the balance
	on the card is within $1,000 of the credit limit. For each user/signer, show Id and Name (see
	the expected output for an example).
	*/
	
	public List<PersonBO[]> query1()
	{
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT ").append(
					 " U.ID AS AUTHUSERID, U.NAME AS AUTHUSERNAME, ").append(
					 " S.ID AS SIGNERID, S.NAME AS SIGNERNAME ").append(
					 " FROM ").append(
					 " PERSON U, PERSON S, CREDITCARD C, ORGANIZATION D").append(
					 " WHERE ").append(
					 " C.CARD_OWNER = D.OID AND ").append(
					 " (C.CARD_LIMIT - C.BALANCE) <= 1000 AND ").append(
					 " U.ID = ANY(C.AUTHUSER) AND ").append(
					 " S.ID = ANY(D.SIGNERS)").append(
					 " ORDER BY U.ID");
		String sql = sqlBuilder.toString();
		List<PersonBO[]> personList = new ArrayList<PersonBO[]>();
		try {
			pstmt = connection.prepareStatement(sql);
			rS = pstmt.executeQuery();
			PersonBO[] personArray;
			while (rS.next())
			{
				personArray = new PersonBO[2];
				personArray[0] = new PersonBO();
				personArray[0].setId(rS.getString("AUTHUSERID"));
				personArray[0].setName(rS.getString("AUTHUSERNAME"));
				personArray[1] = new PersonBO();
				personArray[1].setId(rS.getString("SIGNERID"));
				personArray[1].setName(rS.getString("SIGNERNAME"));
				personList.add(personArray);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {		
			closeSQL();
		}
		return personList;
	}
	
	/*
	2. Find all users (Id, Name) who own four or more cards and are authorized non-owner users for
	three or more other cards. This query must use aggregates.
	*/
	public List<PersonBO> query2()
	{
		List<PersonBO> resultList = new ArrayList<PersonBO>();
		StringBuilder sqlBuilder = new StringBuilder();
		StringBuilder innerQueryBuilder = new StringBuilder();
		innerQueryBuilder.append(" SELECT D.NON_OWNER ").append( 
								 " FROM AUTHNONOWNER D ").append( 
								 " GROUP BY D.NON_OWNER ").append(
								 " HAVING COUNT(D.ACCOUNT_NUMBER) >= 3");
		sqlBuilder.append("SELECT ").append(
					      " C.Id AS PERSONID, C.Name AS PERSONNAME").append(
					      " FROM CREDITCARD B, PERSON C").append( 
					      " WHERE C.OID = B.CARD_OWNER AND").append( 
					      " C.ID IN (").append(innerQueryBuilder).append(
					      " ) GROUP BY").append( 
					      " C.Id, C.Name").append( 
					      "	HAVING COUNT(B.ACCOUNT_NUMBER) >= 4");
		try 
		{
			pstmt = connection.prepareStatement(sqlBuilder.toString());
			rS = pstmt.executeQuery();
			while (rS.next()) 
			{
				PersonBO pBO = new PersonBO();
				pBO.setId(rS.getString("PERSONID"));
				pBO.setName(rS.getString("PERSONNAME"));
				resultList.add(pBO);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			closeSQL();
		}
		return resultList;
	}
	
	/*
	3. Find the credit cards (acct. numbers) all of whose signers (i.e., people with signature authority
	of the organizations that own those cards) also own personal credit cards with credit limits at
	least $25,000. This query must use quantiers.
	*/
	public List<String> query3()
	{
		List<String> creditCardList = new ArrayList<String>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT CC.ACCOUNT_NUMBER AS ACCOUNTNUMBER").append(
						  " FROM CREDITCARD CC, ORGANIZATION I").append(
						  " WHERE CC.CARD_OWNER = I.OId").append(
						  " AND NOT EXISTS (  ").append(
								  " SELECT P.Id").append(
								  " FROM PERSON P").append(
								  " WHERE P.ID = ANY(I.SIGNERS)").append( 
								  " AND NOT EXISTS ( ").append(
										  " SELECT C.ACCOUNT_NUMBER").append(
										  " FROM CREDITCARD C").append(
										  " WHERE C.CARD_OWNER = P.OId").append( 
										  " AND C.CARD_LIMIT >= 25000))");
		
		try {
			pstmt = connection.prepareStatement(sqlBuilder.toString());
			rS = pstmt.executeQuery();
			while (rS.next()) 
			{
				creditCardList.add(rS.getString("ACCOUNTNUMBER"));
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			closeSQL();
		}
		return creditCardList;
	}
	
	/*
	4. Find all pairs (U,C) where U is an indirect user of the credit card C. (For each user, show Id and
	Name. For credit cards, show the account number.)
	A user U is an indirect user of a credit card C if either
	 U is a direct user, i.e., U is one of the authorized users of C; or
	 U is a direct user of a credit card C2 and the owner of C2 is an indirect user of C.
	This query involves recursion.
	*/
	public List<String[]> query4()
	{
		List<String[]> returnList = new ArrayList<String[]>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT I.USERID AS UID, I.USERNAME AS UNAME, I.CARD AS ACCOUNTNUMBER ").append(
						  " FROM INDIRECTUSER I").append(
						  " ORDER BY I.USERID,I.USERNAME,I.CARD");
		try {
			pstmt = connection.prepareStatement(sqlBuilder.toString());
			rS = pstmt.executeQuery();
			while (rS.next()) 
			{
				String[] data = new String[3];
				data[0] = rS.getString("UID");
				data[1] = rS.getString("UNAME");
				data[2] = rS.getString("ACCOUNTNUMBER");
				returnList.add(data);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			closeSQL();
		}
		return returnList;
	}
	
	/*
	5. Find the total of all balances for the credit cards that have Joe as one of the indirect users. This
	query can (and should) reuse some of the earlier queries.
	*/
	public double query5()
	{
		double totalBalance = 0.0D;
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT SUM(C.BALANCE) AS TOTALBALANCE ").append(
						  " FROM CREDITCARD C, INDIRECTUSER I ").append(
						  " WHERE I.USERNAME = 'Joe' AND I.CARD = C.ACCOUNT_NUMBER" );
		try {
			pstmt = connection.prepareStatement(sqlBuilder.toString());
			rS = pstmt.executeQuery();
			if (rS.next())
				totalBalance = rS.getDouble("TOTALBALANCE");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			closeSQL();
		}
		return totalBalance;
	}
	
	/**
	 * This method gets all the records from the OWNER table
	 * Not Implemented (outside scope of project)
	 */
	public List<OwnerBO> getOwnerData()
	{
		List<OwnerBO> ownerList = new ArrayList<OwnerBO>();
		return ownerList;
	}
	
	/**
	 * This method gets all the records from the PERSON table
	 * Not Implemented (outside scope of project)
	 */	
	public List<PersonBO> getPersonData()
	{
		List<PersonBO> personList = new ArrayList<PersonBO>();
		return personList;
	}
	
	/**
	 * This method gets all the records from the ORGANIZATION table
	 * Not Implemented (outside scope of project)
	 */
	public List<OrganizationBO> getOrganizationData()
	{
		List<OrganizationBO> organizationList = new ArrayList<OrganizationBO>();
		return organizationList;
	}
	
	/**
	 * This method gets all the records from the CREDITCARD table
	 * Not Implemented (outside scope of project)
	 */
	public List<CreditCardBO> getCreditCardList()
	{
		List<CreditCardBO> creditCardList = new ArrayList<CreditCardBO>();
		return creditCardList;
	}
	
	/**
	 * This method closes the resultset and preparedstatements for housekeeping purposes
	 * Not Implemented (outside scope of project)
	 */
	public void closeSQL()
	{
		try {
			if (rS != null) {
				rS.close();
				rS = null;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (pstmt != null) {
				pstmt.close();
				pstmt = null;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
