/****************************************************************************
CSE532 -- Project 2

File name: 		CreditCardDBAccess.java
Author(s): 		Jugu Dannie Sundar (110455279) 
		   		Saiyang Qi (110617240)
Description:  	This servlet handles all ajax calls from the end user
				to show the results of the appropriate queries
					
					
We, pledge our honor that all parts of this project were done by us alone 
and without collaboration with anybody else.
****************************************************************************/

package com.creditcard.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.creditcard.common.CreditCardDBAccess;
import com.creditcard.service.CreditCardService;
import com.creditcard.service.RequestedResource;

/**
 * Servlet implementation class CreditCardServlet
 */
public class CreditCardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public CreditCardServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response); // delegating get request to the post method
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		CreditCardDBAccess creditCardAccessObject = null;
		String resultString = "";
		try 
		{
			creditCardAccessObject = new CreditCardDBAccess();
			// creating new connection using CreditCardAccessObject
			CreditCardService ccService = new CreditCardService(creditCardAccessObject.getConnection());
			RequestedResource reqResource = new RequestedResource(request);
			resultString = ccService.getQueryResult(reqResource);
		}
		finally 
		{
			if (creditCardAccessObject != null) // closing connection once the results are returned after ajax call
			{
				creditCardAccessObject.closeConnection();
			}
			response.getWriter().write(resultString);
		}
	}

}
