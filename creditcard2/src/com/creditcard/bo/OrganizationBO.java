/****************************************************************************
CSE532 -- Project 2

File name: 		OrganizationBO.java
Author(s): 		Jugu Dannie Sundar (110455279) 
		   		Saiyang Qi (110617240)
Description:  	This business object stores an Organization information.
				This class extends Owner class
					
					
We, pledge our honor that all parts of this project were done by us alone 
and without collaboration with anybody else.
****************************************************************************/
package com.creditcard.bo;

public class OrganizationBO extends OwnerBO{
	private String[] signers;
	public String[] getSigners() {
		return signers;
	}
	public void setSigners(String[] signers) {
		this.signers = signers;
	}
	public OrganizationBO() {
	}
}
