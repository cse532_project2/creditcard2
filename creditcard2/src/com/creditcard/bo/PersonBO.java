/****************************************************************************
CSE532 -- Project 2

File name: 		PersonBO.java
Author(s): 		Jugu Dannie Sundar (110455279) 
		   		Saiyang Qi (110617240)
Description:  	This business object stores a person's information from PERSON table.
				This class extends Owner class
					
					
We, pledge our honor that all parts of this project were done by us alone 
and without collaboration with anybody else.
****************************************************************************/

package com.creditcard.bo;

public class PersonBO extends OwnerBO{
	private String dob;
	
	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	

	public PersonBO() {
	}

}
