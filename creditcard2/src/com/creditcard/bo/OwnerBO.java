/****************************************************************************
CSE532 -- Project 2

File name: 		OwnerBO.java
Author(s): 		Jugu Dannie Sundar (110455279) 
		   		Saiyang Qi (110617240)
Description:  	This business object stores a owner's information from OWNER table
					
					
We, pledge our honor that all parts of this project were done by us alone 
and without collaboration with anybody else.
****************************************************************************/
package com.creditcard.bo;

import org.json.JSONObject;

public class OwnerBO {
	private String id;
	private String Name;
	private Address Address;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public Address getAddress() {
		return Address;
	}
	public void setAddress(Address address) {
		Address = address;
	}
	public JSONObject getJSONObject() throws Exception {
		JSONObject jsObj = new JSONObject();
		jsObj.put("id", this.getId());
		jsObj.put("name", this.getName());
		jsObj.put("address", this.getAddress());
		return jsObj;
	}
	
	public JSONObject getAddressJSON() throws Exception {
		JSONObject addrJSON = new JSONObject();
		return addrJSON;
	}
}
