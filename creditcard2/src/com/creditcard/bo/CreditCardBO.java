/****************************************************************************
CSE532 -- Project 2

File name: 		CreditCardBO.java
Author(s): 		Jugu Dannie Sundar (110455279) 
		   		Saiyang Qi (110617240)
Description:  	This business object stores a credit card information.
					
					
We, pledge our honor that all parts of this project were done by us alone 
and without collaboration with anybody else.
****************************************************************************/
package com.creditcard.bo;

public class CreditCardBO {
	private String accountNumber;
	private String ownerId;
	private double balance;
	private double limit;
	public CreditCardBO() {
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public double getLimit() {
		return limit;
	}
	public void setLimit(double limit) {
		this.limit = limit;
	}

}
