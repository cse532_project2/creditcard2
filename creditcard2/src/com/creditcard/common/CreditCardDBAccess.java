/****************************************************************************
CSE532 -- Project 2

File name: 		CreditCardDBAccess.java
Author(s): 		Jugu Dannie Sundar (110455279) 
		   		Saiyang Qi (110617240)
Description:  	This class creates a database connection
					
					
We, pledge our honor that all parts of this project were done by us alone 
and without collaboration with anybody else.
****************************************************************************/

package com.creditcard.common;

import java.sql.Connection;
import java.sql.DriverManager;


public class CreditCardDBAccess {

	private Connection connection;
	public CreditCardDBAccess() {
		// TODO Auto-generated constructor stub
		try {
			//Class.forName("org.postgresql.driver");
			DriverManager.registerDriver(new org.postgresql.Driver());
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CreditCardDatabase","postgres","admin");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public Connection getConnection() {
		return connection;
	}
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public void closeConnection() {
		try {
			if (connection != null)
				connection.close();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		finally {
			connection = null;
		}
	}

}
