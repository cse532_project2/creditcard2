﻿/****************************************************************************
CSE532 -- Project 2

File name: 		creditcard.sql
Author(s): 		Jugu Dannie Sundar (110455279) 
		   		Saiyang Qi (110617240)
Description:  	This file contains all the DDLs and DMLs for the project 
					
					
We, pledge our honor that all parts of this project were done by us alone 
and without collaboration with anybody else.
****************************************************************************/

/**
DDL QUERIES FOR BUILDING DATABASE FOR CREDIT CARD PROJECT
*/

DROP VIEW IF EXISTS INDIRECTUSER;
DROP VIEW IF EXISTS DIRECTUSER;
DROP VIEW IF EXISTS AUTHNONOWNER;
DROP TABLE IF EXISTS CREDITCARD;
DROP TYPE IF EXISTS CREDITCARDTYPE;
DROP TABLE IF EXISTS ORGANIZATION;
DROP TABLE IF EXISTS PERSON;
DROP TABLE IF EXISTS OWNER;
DROP TYPE IF EXISTS OWNERTYPE;
DROP TYPE IF EXISTS ADDRESSTYPE;

CREATE TYPE ADDRESSTYPE AS (
STREETNUMBER INT,
STREETNAME VARCHAR(50),
ZIPCODE VARCHAR(5),
STATE VARCHAR(5));

CREATE TYPE OWNERTYPE AS(
ID VARCHAR(10),
NAME VARCHAR(50),
ADDRESS ADDRESSTYPE);

CREATE TABLE OWNER OF OWNERTYPE (
PRIMARY KEY (id),
CHECK (NAME <> '' AND NAME <> NULL)) WITH OIDS;

CREATE TABLE PERSON( 
DOB VARCHAR(10),
PRIMARY KEY (id))
INHERITS (OWNER) WITH OIDS;


CREATE TABLE ORGANIZATION (
SIGNERS VARCHAR(10)[],
PRIMARY KEY (id))
INHERITS (OWNER) WITH OIDS;


CREATE TYPE CREDITCARDTYPE AS (
ACCOUNT_NUMBER VARCHAR(20),
CARD_OWNER OID,
BALANCE NUMERIC(10,2),
CARD_LIMIT NUMERIC(10,2),
AUTHUSER VARCHAR(10)[]);

CREATE TABLE CREDITCARD OF CREDITCARDTYPE(
PRIMARY KEY(ACCOUNT_NUMBER),
CHECK (CARD_LIMIT > 0)
) WITH OIDS;


/**
DML QUERIES FOR BUILDING DATABASE
*/
DELETE FROM CREDITCARD;
DELETE FROM ORGANIZATION;
DELETE FROM PERSON;
DELETE FROM OWNER;



/*
	Insert scripts for PERSON table
*/
INSERT INTO PERSON(id, name, address, dob) VALUES('p1','Ann',ROW(3300,'123 Street','30144','GA'),'20151010');
INSERT INTO PERSON(id, name, address, dob) VALUES('p2','John',ROW(3301,'123 Street','30144','GA'),'20151010');
INSERT INTO PERSON(id, name, address, dob) VALUES('p3','Joe',ROW(3302,'123 Street','30144','GA'),'20151010');
INSERT INTO PERSON(id, name, address, dob) VALUES('p4','May',ROW(3303,'123 Street','30144','GA'),'20151010');
INSERT INTO PERSON(id, name, address, dob) VALUES('p5','Bob',ROW(3304,'123 Street','30144','GA'),'20151010');
INSERT INTO PERSON(id, name, address, dob) VALUES('p6','Faye',ROW(3305,'123 Street','30144','GA'),'20151010');
INSERT INTO PERSON(id, name, address, dob) VALUES('p7','Bill',ROW(3306,'123 Street','30144','GA'),'20151010');
INSERT INTO PERSON(id, name, address, dob) VALUES('p8','Sally',ROW(3307,'123 Street','30144','GA'),'20151010');
INSERT INTO PERSON(id, name, address, dob) VALUES('p9','Miriam',ROW(3308,'123 Street','30144','GA'),'20151010');
INSERT INTO PERSON(id, name, address, dob) VALUES('p10','April',ROW(3309,'123 Street','30144','GA'),'20151010');

/*
	Insert scripts for ORGANIZATION table
*/
INSERT INTO ORGANIZATION(id, name, address, signers) VALUES('o1','Acme',ROW(1,'Infinite Loop','30144','CA'),'{"p3","p4"}');
INSERT INTO ORGANIZATION(id, name, address, signers) VALUES('o2','GoodMood',ROW(731,'Lexinton Ave.','30144','NY'),'{"p1","p4"}');
INSERT INTO ORGANIZATION(id, name, address, signers) VALUES('o3','SuperiorCruft',ROW(1701,'JFK Boulevard','30144','PA'),'{"p5"}');
INSERT INTO ORGANIZATION(id, name, address, signers) VALUES('o4','AwesomeProducts',ROW(2300,'West Plano Parkway','30144','TX'),'{"p1","p6","p7"}');

/* 
	Insert scripts for CREDITCARD table
*/


INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c1',oid,10001,11000,'{"p8","p9"}' from organization where id='o1';
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c2', oid, 18000, 26000, '{"p2","p8","p9"}' from organization where id='o2';
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c3', oid, 27500, 28000, '{"p8","p9"}' from organization where id='o3';
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c4', oid, 26500, 27200, '{"p10"}' from organization where id='o4';
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c5', oid, 1000, 26000, '{"p7"}' from person where id='p1';
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c6', oid, 5000, 28000, null from person where id='p3';                    
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c7', oid, 5000, 20000, null from person where id='p5';                                        
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c8', oid, 5000, 26000, null from person where id='p4';                                        
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c9', oid, 3000, 25000, null from person where id='p6';                                        
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c10', oid, 4000, 30000, '{"p3","p4"}' from person where id='p7';                    
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c11', oid, 4000, 30000, null from person where id='p9';                    
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c12', oid, 4000, 30000, '{"p6"}' from person where id='p10';                    
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c13', oid, 1000, 10000, null from person where id='p9';                    
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c14', oid, 2000, 10000, null from person where id='p9';                    
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c15', oid, 2000, 50000, null from person where id='p9';                    
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c16', oid, 2000, 24000, null from person where id='p8';                                        
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c17', oid, 0, 24000, null from person where id='p8';                                        
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c18', oid, 110, 2000, null from person where id='p8';                                        
INSERT INTO CREDITCARD(account_number, card_owner, balance, card_limit,authuser)
    SELECT 'c19', oid, 220, 2000, null from person where id='p8';                                        

/*
	Creating Authorized non owner view. This is used in queries 2 and 4
*/

CREATE VIEW AUTHNONOWNER (ACCOUNT_NUMBER, NON_OWNER) AS (
SELECT C.ACCOUNT_NUMBER, UNNEST(D.SIGNERS) FROM CREDITCARD C, ORGANIZATION D WHERE (C.card_owner = D.oid)
UNION 
SELECT C.ACCOUNT_NUMBER, UNNEST(C.AUTHUSER) FROM CREDITCARD C, PERSON D WHERE (C.card_owner = D.oid)
UNION
SELECT C.ACCOUNT_NUMBER, UNNEST(C.AUTHUSER) FROM CREDITCARD C, ORGANIZATION D WHERE (C.card_owner = D.oid)
ORDER BY ACCOUNT_NUMBER);

/*Direct users of credit cards */
CREATE VIEW DIRECTUSER(USERID,USERNAME,CARD) AS
SELECT P.Id, P.NAME, N.ACCOUNT_NUMBER
FROM PERSON P, AUTHNONOWNER N
WHERE N.NON_OWNER = P.Id;     

/*Indirect users of credit cards */
CREATE RECURSIVE VIEW INDIRECTUSER(USERID,USERNAME,CARD) AS
SELECT * FROM DIRECTUSER D        
UNION
SELECT D.USERId, D.USERNAME, I.CARD
FROM CREDITCARD C, INDIRECTUSER I, DIRECTUSER D, PERSON P
WHERE D.CARD = C.ACCOUNT_NUMBER
      AND C.CARD_OWNER = P.OID
      AND P.ID = I.USERID; 


